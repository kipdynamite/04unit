# Temat
Testy jednostkowe.

# Cel
Celem zajęć jest wyjaśnienie techniki testów jednostkowych oraz wprowadzenie do popularnych bibliotek programistycznych. W ramach zajęć zostaną wykonane ćwiczenia polegające na tworzeniu i uruchamianiu testów oraz analizie wyników testowania. Praca w grupach, podział według poziomu zaawansowania w zakresie programowania.

# Technologie
*  Java 8, JUnit 5.x
*  Python 3.x, pytest, unittest
*  Angular 7.x, typescript

## Wprowadzenie
Każdy przypadek testowy (test case) jest metodą, funkcją lub procedurą klasy testującej lub skryptu testującego. Zależnie od technologii można spotkać różne elementy składniowe, które wyróżniają testy i ich właściwości (przypadek testowy, ustawianie stanu systemu czy sprawdzanie wyniku). Korzystając z poglądowego projektu z implementacją stosu, należy przeanalizować kody testowe dla wybranej technologii:
* https://gitlab.com/spio-sources/stack/blob/master/pstack/tests/pytest_StackTest.py
* https://gitlab.com/spio-sources/stack/blob/master/astack/src/app/stack/stack.spec.ts
* https://gitlab.com/spio-sources/stack/blob/master/jstack/stack-app/src/test/java/pl/poznan/put/spio/StackTest.java

Ogólny opis projektu z implementacją stosu: https://gitlab.com/spio-sources/stack

Niezależnie od wybranej technologii przypadki testowe z reguły zawierają w sobie te same elementy:
* przygotowanie danych i stanu systemu,
* wywołanie testowanego kodu,
* sprawdzenie uzyskanego wyniku z wartością oczekiwaną.

## Zadanie 1
W ramach projektu https://gitlab.com/spio-sources/translator/ została stworzona klasa/skrypt do szyfrowania kodem typu GADERYPOLUKI (pl.wikipedia.org/wiki/Gaderypoluki). Należy uzupełnić testy jednostkowe translatora, wykorzystując wstępnie przygotowane fragmenty kodu. Testy należy uzupełniać w wybranej przez siebie technologii (Java, Python lub Angular):
* https://gitlab.com/spio-sources/translator/-/blob/master/ptranslator/tests/unittest_gaderypolukiTest.py
* https://gitlab.com/spio-sources/translator/-/blob/master/atranslator/src/app/gaderypoluki/gaderypoluki.spec.ts
* https://gitlab.com/spio-sources/translator/-/blob/master/jtranslator/translator/src/test/java/pl/poznan/put/spio/unit/GaDeRyPoLuKiTest.java

Zakres testów można ustalić wykorzystując techniki doboru danych (np. klasy równoważności).